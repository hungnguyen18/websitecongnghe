﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hightech.Models
{
    public class sanpham
    {
        public string Masp { get; set; }
        public string Tensp { get; set; }
        public string Thuonghieu { get; set; }
        public string Loai { get; set; }
        public string TGBH { get; set; }
        public int Gia { get; set; }
        public string Hinh { get; set; }
        public string Mota { get; set; }
    }
}