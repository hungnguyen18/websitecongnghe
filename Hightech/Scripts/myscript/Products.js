﻿$("#btadd").click(() => {
    $.ajax({
        url: "/HOME/IS_Browse",
        data: {
            
            Tensp: $("#tensp").val(),
            Thuonghieu: $("#thuonghieu").val(),
            Loai: $("#maloai").val(),
            TGBH: $("#tgbh").val(),
            Gia: $("#gia").val(),
            Hinh: $("#img").val(),
            Mota: $("#mota").val()
        },
        type: "POST"
    }).then((res) => {
        console.log(res);
        if (res == "ok") {
            window.location.reload();
            alert("Thêm sản phẩm thành công");
        }
        else {
            alert("Thêm Sản phẩm thất bại");
        }
    });
})

function fndel(Masp) {
    console.log(Masp);
    console.log(`masp_${Masp}`);
    $("#ma").text($(`#masp_${Masp}`).text());
    $("#ten").text($(`#tensp_${Masp}`).text());
}

$("#yes").click(() => {
    $.ajax({
        url: "/Home/Del_Browse",
        data: {
            Masp: $("#ma").text()
        },
        type: "post"
    }).then((res) => {
        console.log(res);
        if (res == "ok") {
            window.location.reload();
        }
        else {
            alert("xóa sản phẩm thất bại !");
        }

    });
})