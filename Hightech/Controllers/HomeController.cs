﻿using Hightech.ConnectDB;
using Hightech.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hightech.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Browse()
        {
            List<sanpham> sp = new List<sanpham>();
            string query = @"store_viewpr";

            DataTable data = connectDB.ExcuteQuery(query);
            for (int i = 0; i < data.Rows.Count; i++)
            {
                sp.Add(new sanpham()
                {
                    Masp = data.Rows[i][0].ToString()
                    ,
                    Tensp = data.Rows[i][1].ToString()
                    ,
                    Loai = data.Rows[i][2].ToString()
                    ,
                    Thuonghieu = data.Rows[i][3].ToString(),

                    TGBH = data.Rows[i][4].ToString(),
                    Hinh = data.Rows[i][5].ToString(),
                    Gia = Convert.ToInt32(data.Rows[i][6]),
                    Mota = data.Rows[i][7].ToString()

                });

            }
            ViewBag.pr = sp;

            return View();
        }
        //thêm sản phẩm
        [HttpPost]
        public ActionResult IS_Browse(sanpham model)
        {
            string query = "store_maxsp";
            DataTable data = connectDB.ExcuteQuery(query);
            model.Masp = ((Convert.ToInt32(data.Rows[0]["maxmasp"]) + 1).ToString().PadLeft(5, '0')).TrimEnd();
            string query1 = "store_products";
            int roweffected = connectDB.ExcuteNonQuery(query1, new Dictionary<string, object>() {
                {"@masp", model.Masp},
                {"@tensp", model.Tensp},
                {"@maloai", model.Loai},
                {"@thuonghieu", model.Thuonghieu},
                {"@tgbh",model.TGBH},
                {"@gia", Request.Form["GIA"]},
                {"@img", model.Hinh},
                {"@mota", model.Mota},
            });

            if (roweffected > 0)
            {
                return Json("ok");
            }
            return Json("error");
        }
        [HttpPost]
        public ActionResult Del_Browse(sanpham model)
        {
            string query = "store_delsp";

            int roweffected = connectDB.ExcuteNonQuery(query, new Dictionary<string, object>() {
                {"@masp", model.Masp.ToString().PadLeft(3,'0')},


            });

            if (roweffected > 0)
            {
                return Json("ok");
            }

            return Json("error");
        }
    }
    
}